/**
  * Created by pasiuga on 5/12/2017.
  */
import org.scalatest._
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class GenericGrouperSpec extends FeatureSpec with GivenWhenThen {

  feature("my first feature"){
    scenario("my frirst scenerio"){

      Given("set 5")
      var x = 5

      When("I increment 5")
      x += 1

      Then("var is 6")
      assert(x === 7)

    }
  }


}
