package dbserch

import spock.lang.Unroll
import spock.lang.Shared
import spock.lang.Specification
import groovy.sql.Sql

class DbSearchTest extends Specification{

	@Shared def url, user, password, driver, sql, rows
	def setupSpec(){
		url = 'jdbc:mysql://localhost:3306/mydb'
		user = 'root'
		password = 'masterkey'
		driver = 'org.gjt.mm.mysql.Driver'
		sql = Sql.newInstance(url, user, password, driver)
		rows = [:]
	}

	def cleanup(){
		rows = [:]
	}
	
	def cleanupSpec(){
		sql.close()
	}

	def 'mySQL test with teachers count'(){
		when:
		rows = sql.rows('select name, surname from teachers')
		println rows

		then:
		rows.size() == 11
	}
	@Unroll
	def 'mySQL test with teachers'() {
		when:
		rows = sql.rows('select name, surname from teachers')
		println rows

		then:
		rows.get(number).name == name
		rows.get(number).surname == surname

		where:
		number||name|surname
		0||'John'|'Romero'
		1||'Phill'|'Rozman'
	}

	def 'mySQL test with teachers and subject'(){
		when:
		rows = sql.rows('select * from teachers where id_subjects = 1')
		println rows

		then:
		def ids = []
		rows.every { it.id == 1 || it.id == 7 } 
		rows.findAll {it.id == 1 && it.id_subjects == 1}.size() == 1
	}
	
	@Unroll
	def 'mySQL test with teachers selected by internal select'(){
		when:
		rows = sql.rows("select name, surname from teachers where id_subjects in (select id from subjects where name = 'mathematics')")
		println rows
		
		then:
		rows.get(number).name == name
		rows.get(number).surname == surname
		
		where:
		number||name|surname
		0||'Eliza'|'Bleck'
		1||'Philippa'|'Kreg'
	}
}
