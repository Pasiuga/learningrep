import unittest

import FirstWebDriverTest

report = open('report.txt', 'w')

loader = unittest.TestLoader()
suite = loader.loadTestsFromModule(FirstWebDriverTest)

runner = unittest.TextTestRunner(verbosity=2)
result = runner.run(suite)

result_str = str(result)

report.write(result_str)
report.close()