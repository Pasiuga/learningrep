import unittest

class ClassA(unittest.TestCase):

    def test_add_a(self):
        self.assertEquals(120, 100 + 20)

    def test_sub_a(self):
        self.assertEquals(210, 230 - 20)

    def test_mul_a(self):
        self.assertEquals(420, 105 * 4)

    def test_sub_test(self):
        self.testsmap = {
            'foo': [1, 1],
            'bar': [1, 2],
            'baz': [5, 5],
            'baf': [5, 6],
        }
        for name, (a, b) in self.testsmap.items():
            with self.subTest(name=name):
                self.assertEqual(a, b, 'Is not equal')


if __name__ == '__main__':
    unittest.main()