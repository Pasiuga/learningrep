import unittest
from xmlrunner import xmlrunner
import FirstWebDriverTest

loader = unittest.TestLoader()
suite = loader.loadTestsFromModule(FirstWebDriverTest)

xmlrunner.XMLTestRunner(verbosity=2, output='test-reports').run(suite)

