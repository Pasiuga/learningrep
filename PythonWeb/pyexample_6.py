import unittest
import sys

import pyexample_6_module_a
import pyexample_6_module_b


suite = unittest.TestLoader().loadTestsFromModule(pyexample_6_module_a)
suite.addTests(unittest.TestLoader().loadTestsFromModule(pyexample_6_module_b))

with open('report.txt', 'w') as report:
    runner = unittest.TextTestRunner(stream=sys.stdout, verbosity=2)
    result = runner.run(suite)
    result_str = str(result)
    report.write(result_str)
