import unittest
from selenium import webdriver

class PythonOrgSearch(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.get("http://www.genecards.org/cgi-bin/carddisp.pl?gene=PDE4B")
        self.fout1 = open('semp.txt', 'w')

    def test_search_in_python_org(self):
        driver = self.driver
        fout1 = self.fout1

        elem = driver.find_element_by_xpath(".//*[@id='aliases_descriptions']/div[1]/div[1]/div[1]/div[2]/ul/li[1]")
        fout1.write(elem.text)


    def tearDown(self):
        self.driver.close()
        self.fout1.close()

if __name__ == '__main__':
    unittest.main()