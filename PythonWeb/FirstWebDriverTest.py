import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys


class PythonOrgSearch(unittest.TestCase):


    @classmethod
    def setUpClass(cls):
        cls.driver = webdriver.Firefox()
        cls.driver.get("http://www.python.org")
        cls.driver.implicitly_wait(30)

    def test_search_in_python_org(self):
        driver = self.driver
        self.assertIn("Python", driver.title)
        elem = driver.find_element_by_name("q")
        elem.send_keys("pycon")
        elem.send_keys(Keys.RETURN)
        find_elem = driver.find_elements_by_xpath("//div[@id='content']/div/section/form/ul/li")
        self.assertEqual(len(find_elem), 20, "No elem")



    @classmethod
    def tearDownClass(cls):
        cls.driver.close()

if __name__ == '__main__':
    unittest.main(verbosity=2)
