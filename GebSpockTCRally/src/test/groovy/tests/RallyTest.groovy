package tests

import geb.spock.GebSpec
import spock.lang.Shared
import org.openqa.selenium.firefox.*

import tests.PageObject.*

class RallyTest extends GebSpec {

	@Shared def browserTest, credentialsFromFile, login, password

	def setupSpec() {
		browserTest = new FirefoxDriver()
		browserTest.manage().window().maximize()
		credentialsFromFile = new File('credentials.txt').text.split(',')
		login = credentialsFromFile[0]
		password = credentialsFromFile[1]
	}

	def setup(){
		browser.driver = browserTest
		to PageLogin
		moduleLoginForm.textboxUserName = login
		moduleLoginForm.textboxPassword = password
		moduleLoginForm.buttonSignIn.click()
		waitFor(30) { at PageMyDashboard }

		moduleMainMenu.buttonRhino.click()
		moduleMainMenu.checkboxParentProjects = true
		moduleMainMenu.buttonRhino.click()

		if (moduleMainMenu.buttonClose.isDisplayed()){
			moduleMainMenu.buttonClose.click()
		}
	}

	def cleanup() {
		moduleMainMenu.buttonForSettings.click()
		waitFor(30) { moduleMainMenu.buttonSignOut }
		moduleMainMenu.buttonSignOut.click()
	}


	def cleanupSpec() {
		browserTest.quit()
	}
	/*
	 def "login to Rally with correct credentials"(){
	 given:  "open login page"
	 to PageLogin
	 when: "I enter correct user name and password"
	 moduleLoginForm.textboxUserName = login
	 moduleLoginForm.textboxPassword = password
	 moduleLoginForm.buttonSignIn.click()
	 then: "My dashboard page is opened"
	 waitFor(30) { at PageMyDashboard }
	 }
	 def "open Kanban Dashboard"(){
	 when: "I click on Portfolio button"
	 moduleMainMenu.buttonPortfolio.click()
	 then: "Portfolio Kanban button is present on drop-down menu"
	 waitFor(30) { moduleMainMenu.buttonPortfolioKanban }
	 when: "I click on Portfolio Kanban button"
	 moduleMainMenu.buttonPortfolioKanban.click()
	 then: "Portfolio Kanban page is opened"
	 waitFor(30) { at PagePortfolioKanban }
	 }
	 def "open Feature page from Release Tracking"(){
	 given: "open Release Tracking board"
	 moduleMainMenu.buttonPortfolio.click()
	 waitFor(30) { moduleMainMenu.buttonReleaseTracking }
	 moduleMainMenu.buttonReleaseTracking.click()
	 waitFor(30) { at PageReleaseTracking }
	 waitFor(30) { linkFeature }
	 when: "I click on link for Feature"
	 linkFeature.click()
	 then: "page for Feature is opened"
	 waitFor(30) { at PageFeature }
	 }
	 def "check number grey card on Portfolio Kanban"(){
	 when: "I click on Portfolio button"
	 moduleMainMenu.buttonPortfolio.click()
	 then: "Portfolio Kanban button is present on drop-down menu"
	 waitFor(30) { moduleMainMenu.buttonPortfolioKanban }
	 when: "I click on Portfolio Kanban button"
	 moduleMainMenu.buttonPortfolioKanban.click()
	 then: "Portfolio Kanban page is opened"
	 waitFor(30) { at PagePortfolioKanban }
	 and: "2 cards are presented"
	 waitFor(30) { cardGrey }
	 cardGrey.size() == 2
	 }
	 def "check opening Print pop-up"(){
	 given: "open Portfolio items page"
	 moduleMainMenu.buttonPortfolio.click()
	 waitFor(30) { moduleMainMenu.buttonPortfolioItems }
	 moduleMainMenu.buttonPortfolioItems.click()
	 waitFor(30) { at PagePortfolioItems }
	 waitFor(30) { buttonImportExportPrint(5) }
	 when: "I click on Import/Export/Print button"
	 buttonImportExportPrint(5).click()
	 then: "Drop-down menu with Print button is opened"
	 waitFor(30) { buttonPrint }
	 }
	 def "check open tested Kanban Board" (){
	 when: "I click on Portfolio button"
	 moduleMainMenu.buttonPortfolio.click()
	 then: "Tested Kanban button is present on drop-down menu"
	 waitFor(30) { moduleMainMenu.buttonNew2Kanban }
	 when: "I click on Tested Kanban button"
	 moduleMainMenu.buttonNew2Kanban.click()
	 then: "Tested Kanban page is opened"
	 waitFor(30) { at PageTested2Board }
	 and: "More, Back, Print, Export buttons are presented"
	 waitFor(90) { buttonAddNew }
	 }
	 */
	def "check work of > button" (int number, String themeName){
		when: "I click on Portfolio button"
		moduleMainMenu.buttonPortfolio.click()

		then: "Portfolio Kanban button is present on drop-down menu"
		waitFor(30) { moduleMainMenu.buttonNew3Kanban }

		when: "I click on Portfolio Kanban button"
		moduleMainMenu.buttonNew3Kanban.click()

		then: "Portfolio Kanban page is opened"
		waitFor(30) { at PageTested3Board }

		when: "I click > button"	
		waitFor(90){
			withFrame(myFrame){ buttonMoreIter(number).click() }
		}
	
		then: "Back to Themes button is present"
		waitFor(90){
			withFrame(myFrame){ textThemeName.text() == themeName }
		}
		
		where:
		number||themeName
		0||"Theme: T475"
		
	}
}
