package tests.PageObject

import geb.Page
import tests.Modules.MainMenuModule

class PageMyDashboard extends Page {
	
	static at = { title == "1A VBC Portfolio › My Dashboard | CA Agile Central" }
	
	static content = {
		moduleMainMenu { module MainMenuModule }
	}
	
}
