package tests.PageObject

import geb.Page
import tests.Modules.MainMenuModule

class PageTested3Board extends Page {
	static at = { title == "1A VBC Portfolio › 6 | CA Agile Central" }
	
	static content = {
		moduleMainMenu { module MainMenuModule }
		myFrame(page: PageTestedFrame) { $('iframe') }
	}

}
