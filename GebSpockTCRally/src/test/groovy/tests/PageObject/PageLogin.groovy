package tests.PageObject

import geb.Page
import tests.Modules.LoginFormModule

class PageLogin extends Page {

	static url = "https://rally1.rallydev.com/slm/login.op"

	static at = { title == "CA Agile Central Login" }

	static content = {
		moduleLoginForm { module LoginFormModule }
		
	}
}
