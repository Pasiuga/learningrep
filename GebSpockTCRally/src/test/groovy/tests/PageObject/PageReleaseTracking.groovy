package tests.PageObject

import geb.Page
import tests.Modules.MainMenuModule

class PageReleaseTracking extends Page {
	
	static at = { title == "Rhino › Release Tracking | CA Agile Central" }
	
	static content = {
		moduleMainMenu { module MainMenuModule }
		linkFeature { $("a", class: "id feature-link") }
	}

}
