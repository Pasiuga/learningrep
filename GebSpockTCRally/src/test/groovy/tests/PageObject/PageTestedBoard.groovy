package tests.PageObject

import geb.Page
import tests.Modules.MainMenuModule

class PageTestedBoard extends Page {
	static at = { title == "1A Value Streams › 1 | CA Agile Central" }
	
	static content = {
		moduleMainMenu { module MainMenuModule }
		myFrame(page: PageTestedFrame) { $('iframe') }
	}
}
