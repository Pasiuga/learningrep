package tests.PageObject

import geb.Page
import tests.Modules.MainMenuModule

class PagePortfolioItems extends Page {
	static at = { title == "Rhino › Portfolio Items | CA Agile Central" }
	
	static content = {
		moduleMainMenu { module MainMenuModule }
		buttonImportExportPrint { $("span", class: "x4-btn-inner x4-btn-inner-center") }
		buttonPrint { $("span", text: "Print ...") }
	}

}
