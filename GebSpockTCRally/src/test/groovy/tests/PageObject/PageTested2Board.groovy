package tests.PageObject

import geb.Page
import tests.Modules.MainMenuModule

class PageTested2Board extends Page {
	static at = { title == "Rhino › 2 | CA Agile Central" }
	
	static content = {
		moduleMainMenu { module MainMenuModule }
		buttonAddNew { $("span", text: "+ Add New") }
	}

}
