package tests.PageObject

import geb.Page

class PageTestedFrame extends Page {
	static content = {
		buttonMoreAll { $("span", text: ">") }
		buttonMoreIter { i -> buttonMoreAll[i] }
		
		
		buttonBack { $("a", id: "back-btn") }
		buttonExportToCSV { $("a", id: "rallybutton-1015") }
		buttonPrint { $("a", id: "rallybutton-1014") }
		textThemeName { $("tspan") }
	}
}
