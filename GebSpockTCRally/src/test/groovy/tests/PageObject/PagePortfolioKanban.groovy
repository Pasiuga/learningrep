package tests.PageObject

import geb.Page
import tests.Modules.MainMenuModule

class PagePortfolioKanban extends Page {
	
	static at = { title == "Rhino › Portfolio Kanban | CA Agile Central" }
	
	static content ={
		moduleMainMenu { module MainMenuModule }
		cardGrey { $("div", style: "background-color:#848689;") }
		buttonAddNew { $("span", text: "+ Add New") }
	}

}
