package tests.PageObject

import geb.Page
import tests.Modules.MainMenuModule

class PageFeature extends Page {
	static at = { title == "F4526: Rally Executive Dashboard | CA Agile Central" }
	
	static content = {
		moduleMainMenu { module MainMenuModule }
	}
}
