package tests.Modules

import geb.Module

class MainMenuModule extends Module {
	
	static content = {
		buttonClose (required: false) { $("button", class: "close")  }
		
		buttonRhino { $("a", class: "project-nav-button icon-chevron-down visible") }
		checkboxParentProjects { $("input", id: "project_nav_scope_up") }
		buttonRhinoPlayground { $("a", text: "Rhino Playground") }
		
		
		buttonPortfolio { $("a", title: "Portfolio") }
		buttonPortfolioKanban { $("a", href: contains("portfoliokanban")) }
		buttonReleaseTracking { $("a", href: contains("releasetracking")) }
		buttonPortfolioItems { $("a", href: contains("portfolioitemstreegrid")) }
		buttonNewKanban { $("a", href: contains("custom/70997430084")) }
		buttonNew2Kanban { $("a", href: contains("custom/71326080628")) }
		buttonNew3Kanban { $("a", href: contains("custom/72158072104")) }
		
		buttonForSettings { $("span", class: "icon-chevron-down") }
		buttonSignOut { $("span", text: "Sign Out") }
	}
}