package tests.Modules

import geb.Module

class LoginFormModule extends Module {
	
	static content = {
		textboxUserName { $("input", id: "j_username") }
		textboxPassword { $("input", id: "j_password") }
		buttonSignIn { $("input", id: "login-button") }
	}
}