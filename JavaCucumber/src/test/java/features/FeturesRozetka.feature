@Rozetka
Feature: test rozetka web site

  Background:
    Given I on rozetka start page

  @Check_Logo
  Scenario: test1
    Then Rozetka logo is presented on start page
  @Check_Apple_Submenu
  Scenario: test2
    Then "Apple" sub-menu is presented on start page
  @Check_MP3_Submenu
  Scenario: test3
    Then "MP3" sub-menu is presented on start page
  @Check_CityChooser_Popup
  Scenario: test4
    When I click Select City link
    Then City link is presented:
      |Харьков|
      |Одесса|
      |fldsfs|
  @Check_Basket_Popup
  Scenario: test5
    When I click Busket button
    Then Empty Busket pup-up is opened
