package runner;

import MappingRozetka.MainRozetka;
import MappingStackoverflow.*;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

/**
 * Created by pasiuga on 6/8/2016.
 * for a) @Rozetka, @Stackoverflow
 * for b) @Check_Logo, @Check_CityChooser_Popup, @Check_FeaturedCont, @Check_GoogleFaceBook_Buttons
 * for c) @Rozetka
 * for d) @Stackoverflow
 */
@RunWith(Cucumber.class)
@CucumberOptions(
        glue = {"steps"},
        features = {"src/test/java/features"},
        tags = {"@Check_Logo"}
)
public class MyRunner {
    public static WebDriver driver;
    public static MainRozetka mainRozetka;
    public static String rozetkaUrl = "http://rozetka.com.ua/";
    public static MainStackoverflow mainStackoverflow;
    public static String stackoverflowUrl = "http://stackoverflow.com/";
    public static SignUp signUp;
    public static TopQuestion topQuestion;

    @BeforeClass
    public static void setUp() throws Exception{
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        mainRozetka = new MainRozetka(driver);
        mainStackoverflow = new MainStackoverflow(driver);
        signUp = new SignUp(driver);
        topQuestion = new TopQuestion(driver);
    }

    @AfterClass
    public static void tearDown() throws Exception{
        driver.close();
    }
}
