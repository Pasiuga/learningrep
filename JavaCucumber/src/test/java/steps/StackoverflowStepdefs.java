package steps;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import runner.MyRunner;

/**
 * Created by pasiuga on 6/8/2016.
 */
public class StackoverflowStepdefs {
    @Given("^I on stackoverflow start page$")
    public void iOnStackoverflowStartPage() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        MyRunner.driver.get(MyRunner.stackoverflowUrl);
    }

    @Then("^Number in featured tub is bigger then \"([^\"]*)\"$")
    public void numberInFeaturedTubIsBiggerThen(int arg0) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        MyRunner.mainStackoverflow.featuredCountAmount(arg0);
    }

    @When("^I click Sign Up button$")
    public void iClickSignUpButton() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        MyRunner.mainStackoverflow.navigateToSignUp();
    }

    @Then("^Google and Facebook buttons are presented$")
    public void googleAndFacebookButtonsArePresented() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        MyRunner.signUp.buttonsForSignUpDerermine();
    }

    @When("^I click on any top Question link$")
    public void iClickOnAnyTopQuestionLink() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        MyRunner.mainStackoverflow.navigateToTopQuestionPage();
    }

    @Then("^Text today is presented$")
    public void textTodayIsPresented() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        MyRunner.topQuestion.questionDateDetermine();
    }


    @Then("^Price with more then \"([^\"]*)\" is presented$")
    public void priceWithMoreThenIsPresented(int arg0) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        MyRunner.mainStackoverflow.priceWorkDetermine(arg0);
    }
}
