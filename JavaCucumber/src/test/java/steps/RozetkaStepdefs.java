package steps;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import runner.MyRunner;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pasiuga on 6/8/2016.
 */
public class RozetkaStepdefs {

    @Given("^I on rozetka start page$")
    public void iOnRozetkaStartPage() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        MyRunner.driver.get(MyRunner.rozetkaUrl);
    }


    @Then("^Rozetka logo is presented on start page$")
    public void rozetkaLogoIsPresentedOnStartPage() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        MyRunner.mainRozetka.checkLogoRozetka();
    }

    @Then("^\"([^\"]*)\" sub-menu is presented on start page$")
    public void subMenuIsPresentedOnStartPage(String arg0) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        MyRunner.mainRozetka.checkSubmenu(arg0);
    }

    @When("^I click Select City link$")
    public void iClickSelectCityLink() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        MyRunner.mainRozetka.clickOnCitySelectorLink();
    }


    @When("^I click Busket button$")
    public void iClickBusketButton() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        MyRunner.mainRozetka.clickOnBasketButton();
    }

    @Then("^Empty Busket pup-up is opened$")
    public void emptyBusketPupUpIsOpened() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        MyRunner.mainRozetka.checkBasketMessage();
    }

    @Then("^\"([^\"]*)\" links are presented$")
    public void linksArePresented(String arg0) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        MyRunner.mainRozetka.checkCityList(arg0);
    }


    @Then("^City link is presented:$")
    public void cityLinkIsPresented(DataTable dataTable) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        List<String> actualCity = new ArrayList<String>();
        actualCity = dataTable.asList(String.class);
        for (String city: actualCity){
            MyRunner.mainRozetka.checkCityList(city);
        }


    }
}
