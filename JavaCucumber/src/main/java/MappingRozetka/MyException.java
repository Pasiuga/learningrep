package MappingRozetka;

import org.openqa.selenium.NoSuchElementException;
/**
 * Created by pasiuga on 6/13/2016.
 */
public class MyException extends NoSuchElementException {

    String message = "";
    public MyException(String message) {
        super(message);
        this.message = message;
    }
    @Override
    public String getMessage(){
        return message;
    }
}
