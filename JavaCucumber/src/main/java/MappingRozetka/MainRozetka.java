package MappingRozetka;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


import java.util.ArrayList;
import java.util.List;


/**
 * Created by VOPas on 04.06.2016.
 */
public class MainRozetka {
    private WebDriver driver;

    public MainRozetka (WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

        @FindBy(xpath = ".//*[@id='body-header']//div[@class='logo161616']/img")
        public WebElement logoRozetka;

        @FindBy(xpath = ".//*[@id='m-main']/descendant::a[contains(text(), 'Apple')]")
        public WebElement submenuApple;

        @FindBy(xpath = ".//*[@id='m-main']/descendant::a[contains(text(), 'MP3')]")
        public WebElement submenuMp3;

        @FindBy(xpath = ".//*[@id='city-chooser']/a")
        public WebElement cityChooserLink;

        @FindBy(xpath = ".//*[@class='header-city-i']/child::*")
        public List<WebElement> cityList;

        @FindBy(xpath = ".//*[contains(@id,'cart_block')]/a")
        public WebElement basketLink;

        @FindBy(xpath = ".//*[@id='drop-block']/h2[contains(text(), 'Корзина пуста')]")
        public WebElement emptyBasketMessage;

    public void checkLogoRozetka(){
        try {
            Assert.assertTrue("Logo Rozetka is not displayed", logoRozetka.isDisplayed());
        } catch (NoSuchElementException e) {
            Assert.fail("Incorrect xpath");
        }


    }

    public void checkSubmenu(String submenuName){
        if (submenuName.equals("Apple")){
            Assert.assertTrue("Sub menu Apple is not displayed", submenuApple.isDisplayed());
        }
        else if (submenuName.equals("MP3")){
            Assert.assertTrue("Sub menu with MP3 is not displayed", submenuMp3.isDisplayed());
        } else {
            Assert.fail("Incorrect sub menu name");
        }
    }

    public void clickOnCitySelectorLink(){
        Assert.assertTrue("City selector is not presented", cityChooserLink.isDisplayed());
        cityChooserLink.click();
    }

    public void checkCityList(String cityName){
        boolean suggestion = true;
        ArrayList<String> cityListName = new ArrayList<String>();
        for (int i = 0; i < cityList.size(); i++){
            cityListName.add(cityList.get(i).getText());
        }
        if (cityListName.lastIndexOf(cityName) == -1){
            suggestion = false;
        }
        String assertMessage = "City " + cityName + " is not presented in the list";
        Assert.assertTrue(assertMessage , suggestion);
    }
    public void clickOnBasketButton(){
        Assert.assertTrue("Basket is not displayed", basketLink.isDisplayed());
        basketLink.click();
    }
    public void checkBasketMessage(){
        Assert.assertTrue("Basket is not empty", emptyBasketMessage.isDisplayed());
    }
}
