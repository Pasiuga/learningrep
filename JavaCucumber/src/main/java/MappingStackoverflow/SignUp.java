package MappingStackoverflow;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by VOPas on 05.06.2016.
 */
public class SignUp {
    private WebDriver driver;

    public SignUp(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy (xpath = ".//span[contains(text(),'Google')]")
    public WebElement googleButton;

    @FindBy (xpath = ".//span[contains(text(),'Facebook')]")
    public WebElement facebookButton;

    public void buttonsForSignUpDerermine() throws Exception{
        Assert.assertTrue("Sign Up - Stack Overflow page is not opened", driver.getCurrentUrl().contains("signup"));
        Assert.assertTrue("Google button is absent", googleButton.isDisplayed());
        Assert.assertTrue("Date is not today", facebookButton.isDisplayed());
    }
}
