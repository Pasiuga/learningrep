package MappingStackoverflow;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by VOPas on 05.06.2016.
 */
public class TopQuestion {
    private WebDriver driver;

    public TopQuestion(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy (xpath = ".//*[contains(text(),'today')]")
    public WebElement questionDate;

    public void questionDateDetermine() throws Exception{
        Assert.assertTrue("Question page is not opened", driver.getCurrentUrl().contains("questions"));
        Assert.assertTrue("Date is not today", questionDate.isDisplayed());
    }
}
