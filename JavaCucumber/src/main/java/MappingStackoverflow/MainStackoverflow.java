package MappingStackoverflow;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

/**
 * Created by VOPas on 04.06.2016.
 */
public class MainStackoverflow {
    private WebDriver driver;

    public MainStackoverflow(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy (xpath = ".//*[@id='tabs']/a[@data-shortcut='B']/span")
    public WebElement featuredCount;

    @FindBy (xpath = ".//a[contains(text(),'sign up')]")
    public WebElement signUpButton;

    @FindBy (xpath = ".//div[@id='question-mini-list']/div[1]/div[2]/h3/a")
    public WebElement topQuestionLink;

    @FindBy (xpath = ".//*[contains(text(),'- $')]")
    public List<WebElement> selary;

    public void featuredCountAmount(int arg0) throws Exception{
        Assert.assertTrue("There is no count of featured", featuredCount.isDisplayed());
        String featCountStr = featuredCount.getText();
        int featCountInt = Integer.valueOf(featCountStr);
        Assert.assertTrue("Number of featured is less of 300", featCountInt > arg0);
    }

    public SignUp navigateToSignUp() throws Exception{
        signUpButton.click();
        return new SignUp (driver);
    }

    public TopQuestion navigateToTopQuestionPage() throws Exception{
        topQuestionLink.click();
        return new TopQuestion (driver);
    }

    public void priceWorkDetermine(int arg0) throws Exception{
        List<WebElement> allPrices = selary;
        boolean suggestion = false;
        String link = "", linkSub = "";
        for (int i = 0; i < allPrices.size(); i++){
            link = allPrices.get(i).getText();
            int first = link.indexOf('$');
            int last;
            last = link.lastIndexOf('k');
            if (last == -1){
                last = link.lastIndexOf('K');
            }
            linkSub = link.substring(first+1, last);
            int price = Integer.valueOf(linkSub);
            if (price >= arg0){
                suggestion = true;
                break;
            }
        }
        Assert.assertTrue("No prices more $100k", suggestion);
    }
}
