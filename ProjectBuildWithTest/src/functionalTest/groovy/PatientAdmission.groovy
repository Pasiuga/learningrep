/* ***************************************************************** */
/*                                                                   */
/* IBM Confidential                                                  */
/* OCO Source Materials                                              */
/*                                                                   */
/* (C) Copyright IBM Corp. 2017                                      */
/*                                                                   */
/* The source code for this program is not published or otherwise    */
/* divested of its trade secrets, irrespective of what has been      */
/* deposited with the U.S. Copyright Office.                         */
/*                                                                   */
/* ***************************************************************** */

import com.xlson.groovycsv.PropertyMapper

class PatientAdmission {

    int patientId
    Date admitDate, dischargeDate = new Date()
    float los, pay, chg, netPay
    PropertyMapper admissionUnparsed

    PatientAdmission() {
        this.patientId = patientId
        this.admitDate = admitDate
        this.dischargeDate = dischargeDate
        this.los = los
        this.pay = pay
        this.chg = chg
        this.netPay = netPay
        this.admissionUnparsed = admissionUnparsed
    }

    def setPatientId(String patientIdStr) {
        this.patientId = Integer.valueOf(patientIdStr)
    }

    def setAdmitDate(String admitDateStr) {
        this.admitDate = Date.parse('MM/dd/yyyy H:mm:ss', admitDateStr)
    }

    def setDischargeDate(String dischargeDateStr) {
        this.dischargeDate = Date.parse('MM/dd/yyyy H:mm:ss', dischargeDateStr)
    }

    def setLos(String losStr) {
        this.los = Float.valueOf(losStr)
    }

    def setPay(String payStr) {
        this.pay = Float.valueOf(payStr)
    }

    def setChg(String chgStr) {
        this.chg = Float.valueOf(chgStr)
    }

    def setNetPay(String netPayStr) {
        this.netPay = Float.valueOf(netPayStr)
    }

    def setAdmissionUnparsed(PropertyMapper admissionUnparsedPrMapper) {
        this.admissionUnparsed = admissionUnparsedPrMapper
    }

    def getAdmissionKey() {
        def admissionKey = []
        this.admissionUnparsed.toMap().each { admissionKey.add(it.getKey()) }
        return admissionKey
    }

    def getAdmissionValue() {
        def admissionValue = []
        this.admissionUnparsed.toMap().each { admissionValue.add(it.getValue()) }
        return admissionValue
    }

}
