/* ***************************************************************** */
/*                                                                   */
/* IBM Confidential                                                  */
/* OCO Source Materials                                              */
/*                                                                   */
/* (C) Copyright IBM Corp. 2017                                      */
/*                                                                   */
/* The source code for this program is not published or otherwise    */
/* divested of its trade secrets, irrespective of what has been      */
/* deposited with the U.S. Copyright Office.                         */
/*                                                                   */
/* ***************************************************************** */

class GenericGrouperExecutor {

    def runGenericGrouper(String pathToFile){
        try {
            Process process = Runtime.getRuntime().exec(pathToFile)
            process.waitFor()
            InputStream inputStream = process.getInputStream()
            def streamMessage = inputStream.readLines()
            return streamMessage
            inputStream.close()
        } catch (IOException e) {
            println(e.toString())
        }

    }
}
