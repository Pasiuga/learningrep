/* ***************************************************************** */
/*                                                                   */
/* IBM Confidential                                                  */
/* OCO Source Materials                                              */
/*                                                                   */
/* (C) Copyright IBM Corp. 2017                                      */
/*                                                                   */
/* The source code for this program is not published or otherwise    */
/* divested of its trade secrets, irrespective of what has been      */
/* deposited with the U.S. Copyright Office.                         */
/*                                                                   */
/* ***************************************************************** */

import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Unroll
import org.apache.commons.collections.CollectionUtils

class GenericGrouperSpec extends Specification {
    @Shared
            actualFileAdmission, expectFileAdmission, actualFileMedlink, expectFileMedlink,
            actualPatientClaims, expectPatientClaims, actualClaim, expectClaim,
            actualPatientAdmissions, expectPatientAdmissions, actualAdmission, expectAdmission,
            runnerEngine, readerCsv, actualList, expectList

    def "run jar file"() {
        when:
        runnerEngine = new GenericGrouperExecutor()
        def text = runnerEngine.runGenericGrouper("java -jar dna-project.jar")
        println(text)

        then:
        text != null
    }

    @Unroll
    def "check_that_Adm_medlink.csv_file_contains_fields_as_expected"() {
        setup:
        actualPatientClaims = []
        expectPatientClaims = []
        actualClaim = new PatientClaim()
        expectClaim = new PatientClaim()

        when:
        actualFileMedlink = new File(actual)
        readerCsv = new ReaderCsv(actualFileMedlink)
        actualPatientClaims = readerCsv.readPatientClaimsUnparsed()
        expectFileMedlink = new File(expected)
        readerCsv = new ReaderCsv(expectFileMedlink)
        expectPatientClaims = readerCsv.readPatientClaimsUnparsed()

        and:
        actualClaim = actualPatientClaims.get(0)
        expectClaim = expectPatientClaims.get(0)

        then:
        CollectionUtils.disjunction(actualClaim.getClaimKey(), expectClaim.getClaimKey()) == []

        where:
        actual                                                  | expected
        'src/functionalTest/resources/Adm_medlink+(output).csv' | 'src/functionalTest/resources/Adm_medlink+(output)_claim_ext_tw_new_claim_begin_2_days_next.csv'
        'src/functionalTest/resources/Adm_medlink+(output).csv' | 'src/functionalTest/resources/Adm_medlink+(output)_2_triggers_for_1st_claim.csv'

    }

    @Unroll
    def "check_that_Adm_medlink.csv_file_created_as_expected"() {
        setup:
        actualPatientClaims = []
        expectPatientClaims = []
        actualClaim = new PatientClaim()
        expectClaim = new PatientClaim()
        actualList = []
        expectList = []

        when:
        actualFileMedlink = new File(actual)
        readerCsv = new ReaderCsv(actualFileMedlink)
        actualPatientClaims = readerCsv.readPatientClaimsUnparsed()
        expectFileMedlink = new File(expected)
        readerCsv = new ReaderCsv(expectFileMedlink)
        expectPatientClaims = readerCsv.readPatientClaimsUnparsed()

        and:
        for (int i = 0; i < actualPatientClaims.size(); i++) {
            actualClaim = actualPatientClaims.get(i)
            expectClaim = expectPatientClaims.get(i)
            actualList.add(actualClaim.getClaimValue())
            expectList.add(expectClaim.getClaimValue())
        }

        then:
        CollectionUtils.disjunction(actualList, expectList) == []

        where:
        actual                                                  | expected
        'src/functionalTest/resources/Adm_medlink+(output).csv' | 'src/functionalTest/resources/Adm_medlink+(output)_claim_ext_tw_new_claim_begin_2_days_next.csv'
        'src/functionalTest/resources/Adm_medlink+(output).csv' | 'src/functionalTest/resources/Adm_medlink+(output)_2_triggers_for_1st_claim.csv'

    }

    @Unroll
    def "check_that_admission_output.csv_file_contains_fields_as_expected"() {
        setup:
        actualPatientAdmissions = []
        expectPatientAdmissions = []
        actualAdmission = new PatientAdmission()
        expectAdmission = new PatientAdmission()

        when:
        actualFileAdmission = new File(actual)
        readerCsv = new ReaderCsv(actualFileAdmission)
        actualPatientAdmissions = readerCsv.readPatientAdmissionsUnparsed()
        expectFileAdmission = new File(expected)
        readerCsv = new ReaderCsv(expectFileAdmission)
        expectPatientAdmissions = readerCsv.readPatientAdmissionsUnparsed()

        and:
        actualAdmission = actualPatientAdmissions.get(0)
        expectAdmission = expectPatientAdmissions.get(0)

        then:
        CollectionUtils.disjunction(actualAdmission.getAdmissionKey(), expectAdmission.getAdmissionKey()) == []

        where:
        actual                                               | expected
        'src/functionalTest/resources/admissions_output.csv' | 'src/functionalTest/resources/admissions_output_2_triggers_for_1st_claim_2nd_ext_tw.csv'
        'src/functionalTest/resources/admissions_output.csv' | 'src/functionalTest/resources/admissions_output_2_triggers_for_1st_claim.csv'
    }

    @Unroll
    def "check_that_admission_output.csv_file_created_as_expected"() {
        setup:
        actualPatientAdmissions = []
        expectPatientAdmissions = []
        actualAdmission = new PatientAdmission()
        expectAdmission = new PatientAdmission()
        actualList = []
        expectList = []

        when:
        actualFileAdmission = new File(actual)
        readerCsv = new ReaderCsv(actualFileAdmission)
        actualPatientAdmissions = readerCsv.readPatientAdmissionsUnparsed()
        expectFileAdmission = new File(expected)
        readerCsv = new ReaderCsv(expectFileAdmission)
        expectPatientAdmissions = readerCsv.readPatientAdmissionsUnparsed()

        and:
        for (int i = 0; i < actualPatientAdmissions.size(); i++) {
            actualAdmission = actualPatientAdmissions.get(i)
            expectAdmission = expectPatientAdmissions.get(i)
            actualList.add(actualAdmission.getAdmissionValue())
            expectList.add(expectAdmission.getAdmissionValue())
        }

        then:
        CollectionUtils.disjunction(actualList, expectList) == []

        where:
        actual                                               | expected
        'src/functionalTest/resources/admissions_output.csv' | 'src/functionalTest/resources/admissions_output_2_triggers_for_1st_claim_2nd_ext_tw.csv'
        'src/functionalTest/resources/admissions_output.csv' | 'src/functionalTest/resources/admissions_output_2_triggers_for_1st_claim.csv'
    }

}