/* ***************************************************************** */
/*                                                                   */
/* IBM Confidential                                                  */
/* OCO Source Materials                                              */
/*                                                                   */
/* (C) Copyright IBM Corp. 2017                                      */
/*                                                                   */
/* The source code for this program is not published or otherwise    */
/* divested of its trade secrets, irrespective of what has been      */
/* deposited with the U.S. Copyright Office.                         */
/*                                                                   */
/* ***************************************************************** */

import com.xlson.groovycsv.PropertyMapper

class PatientClaim {
    int patientId, revCode, stdPlace, admissionId, recordIdentifier, recordNumber
    Date svcDate = new Date()
    String recType
    PropertyMapper claimUnparsed

    PatientClaim() {
        this.patientId = patientId
        this.svcDate = svcDate
        this.revCode = revCode
        this.stdPlace = stdPlace
        this.recType = recType
        this.admissionId = admissionId
        this.recordIdentifier = recordIdentifier
        this.recordNumber = recordNumber
        this.claimUnparsed = claimUnparsed
    }

    def setPatientId(String patientIdStr){
        this.patientId = Integer.valueOf(patientIdStr)
    }

    def setDate(String svcDateStr){
        this.svcDate = Date.parse('MM/dd/yyyy H:mm:ss', svcDateStr)
    }

    def setRevCode(String revCodeStr){
        if (revCodeStr == ""){
            this.revCode = 0
        } else {
            this.revCode = Integer.valueOf(revCodeStr)
        }
    }

    def setStdPlace(String stdPlaceStr){
        if (stdPlaceStr == ""){
            this.stdPlace = 0
        } else {
            this.stdPlace = Integer.valueOf(stdPlaceStr)
        }
    }

    def setRecType(String recTypeStr){
        this.recType = recTypeStr
    }

    def setAdmissionId(String admissionIdStr){
        this.admissionId = Integer.valueOf(admissionIdStr)
    }

    def setRecordIdentifier(String recordIdentifierStr){
        this.recordIdentifier = Integer.valueOf(recordIdentifierStr)
    }

    def setRecordNumber(String recordNumberStr){
        this.recordNumber = Integer.valueOf(recordNumberStr)
    }

    def setClaimUnparsed(PropertyMapper claimUnparsedPrMapper){
        this.claimUnparsed = claimUnparsedPrMapper
    }

    def getClaimKey(){
        def claimKey = []
        this.claimUnparsed.toMap().each { claimKey.add(it.getKey())}
        return claimKey
    }

    def getClaimValue(){
        def claimValue = []
        this.claimUnparsed.toMap().each { claimValue.add(it.getValue())}
        return claimValue
    }

}
