/* ***************************************************************** */
/*                                                                   */
/* IBM Confidential                                                  */
/* OCO Source Materials                                              */
/*                                                                   */
/* (C) Copyright IBM Corp. 2017                                      */
/*                                                                   */
/* The source code for this program is not published or otherwise    */
/* divested of its trade secrets, irrespective of what has been      */
/* deposited with the U.S. Copyright Office.                         */
/*                                                                   */
/* ***************************************************************** */

import com.xlson.groovycsv.CsvParser

class ReaderCsv {

    File csvFile

    ReaderCsv(File csvFile) {
        this.csvFile = csvFile
    }

    def readPatientAdmissionsUnparsed() {
        def patientAdmissions = []
        def bufferReader = new BufferedReader(new FileReader(this.csvFile))
        def csvReaded = CsvParser.parseCsv(bufferReader)
        csvReaded.each {
            def patientAdmission = new PatientAdmission()
            patientAdmission.setAdmissionUnparsed(it)
            patientAdmissions.add(patientAdmission)
        }
        bufferReader.close()
        return patientAdmissions
    }

    def readPatientClaimsUnparsed() {
        def patientClaims = []
        def bufferReader = new BufferedReader(new FileReader(this.csvFile))
        def csvReaded = CsvParser.parseCsv(bufferReader)
        csvReaded.each {
            def patientClaim = new PatientClaim()
            patientClaim.setClaimUnparsed(it)
            patientClaims.add(patientClaim)
        }
        bufferReader.close()
        return patientClaims
    }

}
