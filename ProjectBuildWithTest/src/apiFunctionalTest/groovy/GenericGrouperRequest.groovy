/* ***************************************************************** */
/*                                                                   */
/* IBM Confidential                                                  */
/* OCO Source Materials                                              */
/*                                                                   */
/* (C) Copyright IBM Corp. 2017                                      */
/*                                                                   */
/* The source code for this program is not published or otherwise    */
/* divested of its trade secrets, irrespective of what has been      */
/* deposited with the U.S. Copyright Office.                         */
/*                                                                   */
/* ***************************************************************** */

import groovyx.net.http.ContentType
import groovyx.net.http.HTTPBuilder
import groovyx.net.http.Method

class GenericGrouperRequest {
    def ret = null
    def status = null

    private generalRequest(String reqUrl, String path, query, Method method){
        try {
            def http = new HTTPBuilder(reqUrl)
            http.request(method, ContentType.JSON) { request ->
                uri.path = path
                body = query
                requestContentType = ContentType.JSON

                response.success = { resp, reader ->
                    ret = reader
                    println(ret)
                    status = resp.status
                }

                response.failure = { resp, reader ->
                    ret = reader
                    println(ret)
                    status = resp.status
                    println("Request failed with status ${resp.status}")
                }
            }
            return ret
        } catch (Exception ex) {
            ex.printStackTrace()
            return null
        }
    }

    def getPostResponse(String reqUrl, String path, query) {
        generalRequest( reqUrl, path, query, Method.POST)
    }

    def getGetResponse(String reqUrl, String path) {
        generalRequest( reqUrl, path, null, Method.GET)
    }

    def getResponseStatus() {
        return status
    }

}
