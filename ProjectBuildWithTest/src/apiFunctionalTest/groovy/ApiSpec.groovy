/* ***************************************************************** */
/*                                                                   */
/* IBM Confidential                                                  */
/* OCO Source Materials                                              */
/*                                                                   */
/* (C) Copyright IBM Corp. 2017                                      */
/*                                                                   */
/* The source code for this program is not published or otherwise    */
/* divested of its trade secrets, irrespective of what has been      */
/* deposited with the U.S. Copyright Office.                         */
/*                                                                   */
/* ***************************************************************** */

import spock.lang.Shared
import spock.lang.Specification

class ApiSpec extends Specification {
    @Shared
            servUrl, respJson, gaRequest, reqBody, reqPath, respStatus

    def setupSpec() {
        servUrl = new File('src/apiFunctionalTest/resources/url.txt').getText()
        gaRequest = new GenericGrouperRequest()
    }

    def "1)_check_that_GG_listener_takes_correct_response_from_Queue"() {
        when: 'Send_post_request_to_server/AnalyticEngineFactory/request'
        reqPath = '/AnalyticEngineFactory/request'
        reqBody = [parameters: [methodologies: ['ABPG'], medicalClaimFile: ['input_data.csv']]]
        respJson = gaRequest.getPostResponse(servUrl, reqPath, reqBody)
        respStatus = gaRequest.getResponseStatus()

        then: 'obtain_response_with_serviceId'
        respStatus == 200
        respJson.serviceId != null

        when: 'send_get_request_to_server/AnalyticEngineFactory/request/{serviceId}/details'
        sleep(5000)
        def currentId = respJson.serviceId.toInteger()
        reqPath = 'AnalyticEngineFactory/request/' + respJson.serviceId + '/details'

        respJson = gaRequest.getGetResponse(servUrl, reqPath)
        println(respJson)

        then: 'obtain_response_with_status_Success_and_correct_parameters_and_serviceId'
        respJson.request.parameters.methodologies.toString() == '[ABPG:1.0]'
        respJson.request.parameters.medicalClaimFile.toString() == '[input_data.csv]'
        respJson.serviceId.toInteger() == currentId
        respJson.status == 'Success'
    }

    def "2)_check_that_incorrect_response_w/o_\"medicalClaimFile\"_parameter_for_GG_methodology_does_not_go_to_Queue"() {
        when: 'send_post_request_to_server/AnalyticEngineFactory/request'
        reqPath = '/AnalyticEngineFactory/request'
        reqBody = [parameters: [methodologies: ['ABPG']]]
        respJson = gaRequest.getPostResponse(servUrl, reqPath, reqBody)
        respStatus = gaRequest.getResponseStatus()

        then: 'obtain_response_with_errorMessages_and_responseStatus_400'
        respJson.errorMessages.toString() == '[The \'medicalClaimFile\' parameter is mandatory.]'
        respStatus == 400
    }

    def "3)_check_that_incorrect_response_with_incorrect_methodology_does_not_go_to_Queue"() {
        when: 'send_post_request_to_server/AnalyticEngineFactory/request'
        reqPath = '/AnalyticEngineFactory/request'
        reqBody = [parameters: [methodologies: ['gfhfh'], medicalClaimFile: ['input_data.csv']]]
        respJson = gaRequest.getPostResponse(servUrl, reqPath, reqBody)
        respStatus = gaRequest.getResponseStatus()

        then: 'obtain_response_with_errorMessages_and_responseStatus_400'
        respJson.errorMessages.toString() == '[The following Methodologies are either Invalid/Not Supported [gfhfh].]'
        respStatus == 400
    }

    def "4)_check_that_GG_listener_takes_incorrect_response_with_\"medicalClaimFile\"_and_\"enrollmentFile\"_from_Queue"() {
        when: 'send_post_request_to_server/AnalyticEngineFactory/request'
        reqPath = '/AnalyticEngineFactory/request'
        reqBody = [parameters: [methodologies: ['ABPG'], medicalClaimFile: ['input_data.csv'], enrollmentFile: ['enrollmentfortestqme.csv']]]
        respJson = gaRequest.getPostResponse(servUrl, reqPath, reqBody)
        respStatus = gaRequest.getResponseStatus()

        then: 'obtain_response_with_warningMessages_and_serviceId'
        respStatus == 200
        respJson.serviceId != null
        respJson.warningMessages.toString() == '[The following parameters are either Invalid/Not Supported [enrollmentFile]]'

        when: 'send_get_request_to_server/AnalyticEngineFactory/request/{serviceId}/details'
        sleep(5000)
        def currentId = respJson.serviceId.toInteger()
        reqPath = 'AnalyticEngineFactory/request/' + respJson.serviceId + '/details'

        respJson = gaRequest.getGetResponse(servUrl, reqPath)
        println(respJson)

        then: 'obtain_response_with_status_Success_and_correct_parameters_and_serviceId'
        respJson.request.parameters.methodologies.toString() == '[ABPG:1.0]'
        respJson.request.parameters.medicalClaimFile.toString() == '[input_data.csv]'
        respJson.serviceId.toInteger() == currentId
        respJson.status == 'Success'
    }

    def "5)_check_that_GG_listener_does_not_take_response_for_another_PA_methodology"() {
        when: 'send_post_request_to_server/AnalyticEngineFactory/request'
        reqPath = '/AnalyticEngineFactory/request'
        reqBody = [parameters: [methodologies: ['MEGCCM'], medicalClaimFile: ['input_data.csv'], enrollmentFile: ['enrollmentfortestqme.csv']]]
        respJson = gaRequest.getPostResponse(servUrl, reqPath, reqBody)
        respStatus = gaRequest.getResponseStatus()

        then: 'obtain_response_with_serviceId'
        respStatus == 200
        respJson.serviceId != null

        when: 'send_get_request_to_server/AnalyticEngineFactory/request/{serviceId}/details'
        sleep(10000)
        def currentId = respJson.serviceId.toInteger()
        reqPath = 'AnalyticEngineFactory/request/' + respJson.serviceId + '/details'

        respJson = gaRequest.getGetResponse(servUrl, reqPath)
        println(respJson)

        then: 'obtain_response_with_status_Queued_and_correct_parameters_and_serviceId'
        respJson.request.parameters.methodologies.toString() == '[MEGCCM:1.16]'
        respJson.request.parameters.medicalClaimFile.toString() == '[input_data.csv]'
        respJson.request.parameters.enrollmentFile.toString() == '[enrollmentfortestqme.csv]'
        respJson.serviceId.toInteger() == currentId
        respJson.status == 'Error'
    }
}
