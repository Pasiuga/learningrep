import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertTrue;

public class LifetTest {

    private Life life;

    @Before
    public void start() {
        life = new Life(10,10,10,10);
    }

    @Test
    public void getRandomIndexTest() {
        int min = 5;
        int max = 45;
        int cycles = 100000;
        for(int i = 0; i < cycles; i++) {
            int number = life.getRandomIndex(min, max);
            // System.out.println(number);
            assertTrue(number <= max);
            assertTrue(number >= min);
        }

    }

    @After
    public void finish() {
        life = null;
    }

}