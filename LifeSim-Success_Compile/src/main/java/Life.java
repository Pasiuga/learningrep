public class Life{

    private static final char symEmpty = '+';
    private static final char symFull = '#';

    public char[][] plane;
    private int h1;
    private int h2;
    private int amount;
    public int cycleCount;


    public Life(int x, int y, int z, int c){
        h1 = x;
        h2 = y;
        amount = z;
        cycleCount = c;
        plane = new char[h1][h2];
        for(int i = 0; i < h1; i++)
            for(int j = 0; j < h2; j++)
                plane[i][j] = symEmpty;
        fieldRandomizer(amount);
    }

    private void showLine(int lineNumber) {
        for (int i = 0; i < h1; i++) {
            String newString = plane[i][lineNumber] + " ";
            LifeGui.myField.append(newString);
        }
    }

    public void showField() {
        for (int i = 0; i < h2; i++) {
            showLine(i);
            LifeGui.myField.append("\n");
        }
    }

    public void setCell(int x, int y, int symbol, char[][] array){
        if(symbol == 0)
            array[x][y] = symEmpty;
        else array[x][y] = symFull;
    }

    public boolean testPoint(int x, int y){
        return plane[x][y] == symFull;
    }

    public int countNeighbour(int x, int y){
        int sum = 0;
        for(int i = (x - 1); i <= (x + 1); i++)
            for(int j = (y - 1);  j <= (y + 1); j++){
                if(i == x && j == y)
                    continue;
                if(testPoint(i, j))
                    sum += 1;
            }
        return sum;
    }

    public void logic(int x, int y, char[][] array){
        int neib = countNeighbour(x, y);
        if(testPoint(x, y)){
            if(neib > 1){
                if(neib > 3) {
                    setCell(x, y, 0, array);
                } else setCell(x, y, 1, array);
            } else setCell(x, y, 0, array);
        } else if(neib == 3)
            setCell(x, y, 1, array);
        else setCell(x, y, 0, array);
    }

    public void cycleOfLife() {
        char[][] newArray = new char[h1][h2];
        for(int i = 0; i < h1; i++)
            for(int j = 0; j < h2; j++){
                newArray[i][j] = symEmpty;
            }
        for(int i = 1; i < (h1 - 1); i++)
            for(int j = 1; j < (h2 - 1); j++){
                logic(i, j, newArray);
            }
        plane = newArray;
        showField();
    }

    public void fieldRandomizer(int n) {
        for(int i = 0; i < n; i++){
            plane[getRandomIndex(1, h1 - 1)][getRandomIndex(1, h2 - 1)] = symFull;
        }
    }

    public int getRandomIndex(int min, int max) {
        return min + (int)(Math.random()*(max - min));
    }


}