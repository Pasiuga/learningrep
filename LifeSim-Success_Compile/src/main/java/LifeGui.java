import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class LifeGui {

    public JFrame form1  = new JFrame("Life");
    public static JTextArea myField = new JTextArea(50, 50);

    public LifeGui() {

        JButton button = new JButton("Life run");
        final JTextField field1 = new JTextField(5);
        field1.setText("1");
        button.setSize(5, 5);
        JScrollPane scrollPane = new JScrollPane(myField);
        form1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        form1.setLayout(new FlowLayout()); // add GUI elements in order
        form1.add(scrollPane);
        form1.add(field1);
        form1.add(button);
        form1.setExtendedState(JFrame.MAXIMIZED_BOTH);
        //form1.setUndecorated(true); // full screen
        form1.setVisible(true);


        button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Life life = new Life(11,11,50,Integer.parseInt(field1.getText()));
                try {
                    myField.setText("Initial position \n");
                    life.showField();
                    Thread.sleep(2000);
                    for (int i = 1; i < life.cycleCount; i++){
                        String newString1 = "Cycle #" + i + "\n";
                        myField.append(newString1);
                        life.cycleOfLife();
                        myField.append("----------------");
                        Thread.sleep(2000);
                    }
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
            }
        });
    }

   
}
