

import org.openqa.selenium.chrome.ChromeDriver
import org.openqa.selenium.chrome.ChromeOptions
import org.openqa.selenium.firefox.FirefoxDriver

driver = {
	def firefoxDriver = new FirefoxDriver()
	firefoxDriver.manage().window().maximize()
	firefoxDriver
}


environments {

	'firefox' {
		driver = {
			def firefoxDriver = new FirefoxDriver()
			firefoxDriver.manage().window().maximize()
			firefoxDriver
		}
	}

	'chrome' {
		System.setProperty('webdriver.chrome.driver', 'chromedriver/chromedriver.exe')

		driver = {
			def options = new ChromeOptions()
			options.addArguments("--start-maximized")
			def chromeDriver = new ChromeDriver(options)
			chromeDriver 
		}
	}
}

reportsDir = "target/geb-reports"
