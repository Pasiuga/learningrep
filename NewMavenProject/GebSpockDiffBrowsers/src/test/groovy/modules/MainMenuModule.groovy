package modules

import geb.Module

class MainMenuModule extends Module {
	
	static content = {
		linkAboutUs { $("a", title: "About us")}
		linksMainMenu { $("a") }
	}
	

}
