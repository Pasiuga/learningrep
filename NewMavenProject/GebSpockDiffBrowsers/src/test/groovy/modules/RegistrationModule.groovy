package modules

import geb.Module

class RegistrationModule extends Module {
	
	static content = {
		buttonRegistration { $("a", href: "http://demoqa.com/registration/") }
	}

}
