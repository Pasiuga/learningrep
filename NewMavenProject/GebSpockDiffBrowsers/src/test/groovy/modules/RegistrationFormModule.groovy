package modules

import geb.Module

class RegistrationFormModule extends Module{
	static content ={
		textfieldFirstName { $("input", id: "name_3_firstname") }
		selectCountry { $("select", id: "dropdown_7") }
		checkboxHobbyReading { $("input", type: "checkbox", value: "reading") }
		radiobuttonStatusMarried { $("input", type: "radio", value: "married") }
	}

}
