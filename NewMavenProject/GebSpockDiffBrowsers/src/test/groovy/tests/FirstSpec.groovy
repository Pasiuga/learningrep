package tests


import geb.spock.GebReportingSpec


class FirstSpec extends GebReportingSpec{
	
	
/*	
	def "check presence of About Us button via an inline scripting style"() {
		when:
		go "http://demoqa.com/"

		then:
		assert $("a", title: "About us").text() == "About us"
	}
*/
	def "check presence of About Us button"() {
		when:
		to MainMenu

		then:
		sectionAboutUs == "About us"
	}
/*	
	def "check number of Main Menu buttons"(){
		when:
		to MainMenu
		
		then:
		moduleMainMenu.linksMainMenu.size() == 8
	}

	def "check of working About Us button via an inline scripting style"() {
		given:
		go "http://demoqa.com/"
		
		when:
		$("a", title: "About us").click()

		then:
		assert $("h1", class:"entry-title").text() == "About us"
	}

	def "check of working About Us button"(){
		given:
		to MainMenu

		when:
		moduleMainMenu.linkAboutUs.click()

		then:
		at AboutUs
	}

	def "check of working About Us button w/o Modules"(){
		given:
		to MainMenu

		when:
		buttonAboutUs.click()

		then:
		at AboutUs
	}
*/
	def "enter text to text line on Registration Form"(){
		given:
		to MainMenu
		
		when:
		moduleRegistration.buttonRegistration.click()
		
		then:
		at Registration
		
		when:
		moduleRegistrationForm.textfieldFirstName = "Volodymyr"
		
		then:
		moduleRegistrationForm.textfieldFirstName == "Volodymyr"
	}

	def "select item from drop-down menu"(){
		given:
		to MainMenu
		
		when:
		moduleRegistration.buttonRegistration.click()
		
		then:
		at Registration
		
		when:
		moduleRegistrationForm.selectCountry = "Ukraine"
		
		then:
		moduleRegistrationForm.selectCountry == "Ukraine"
	}
	/*
	def "select chexkbox" (){
		given:
		to MainMenu
		
		when:
		moduleRegistration.buttonRegistration.click()
		
		then:
		at Registration
		
		when:
		moduleRegistrationForm.checkboxHobbyReading = true
		
		then:
		moduleRegistrationForm.checkboxHobbyReading.value() == true
	}
	
	def "select radiobutton"(){
		given:
		to MainMenu
		
		when:
		moduleRegistration.buttonRegistration.click()
		
		then:
		at Registration
		
		when:
		moduleRegistrationForm.radiobuttonStatusMarried = true
		
		then:
		moduleRegistrationForm.radiobuttonStatusMarried == true
	}
	*/
}