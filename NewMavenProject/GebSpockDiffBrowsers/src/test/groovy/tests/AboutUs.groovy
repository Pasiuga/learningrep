package tests

import geb.Page

class AboutUs extends Page{
	static at = { title == "About us | Demoqa" }
}
