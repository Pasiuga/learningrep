package tests

import geb.Page
import modules.RegistrationFormModule

class Registration extends Page {
	static at = { title == "Registration | Demoqa" }

	static content ={
		moduleRegistrationForm {module RegistrationFormModule, $("div", class: "entry-content") }
	}
}
