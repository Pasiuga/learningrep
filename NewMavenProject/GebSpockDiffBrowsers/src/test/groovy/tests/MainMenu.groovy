package tests

import geb.Page
import modules.MainMenuModule
import modules.RegistrationModule

class MainMenu extends Page {
	static url = "http://demoqa.com/"
	
	static at = { title == "Demoqa | Just another WordPress site" }
	
	static content = {
		sectionAboutUs { $("a", title: "About us").text() }
		moduleMainMenu { module MainMenuModule, $("div", class: "collapse navbar-collapse navbar-ex1-collapse") }
		moduleRegistration { module RegistrationModule, $("div", id: "secondary") }
		buttonAboutUs { $("a", title: "About us") }
	}
	

}