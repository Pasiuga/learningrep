import spock.lang.Specification

import groovyx.net.http.*
import spock.lang.Shared
import spock.lang.Unroll


class RestSpecificationSpec extends Specification {
	@Shared def testUrl, testQuery, testResponse, http
	def setupSpec(){
		testUrl = "http://api.openweathermap.org"
		testQuery = ['APPID': "b2ce5b9466a4cdcec5e7a6bf11465c5a"]
		testResponse = ''
		http = new HTTPBuilder(testUrl)
	}

	@Unroll
	def 'Check if the weather in Kharkiv can be found'() {

		when:
		testQuery.put('q', cityReq)
		testResponse = http.get(path : '/data/2.5/weather', query : testQuery)
		print testQuery
		println testResponse

		then:
		testResponse.cod == 200

		and:
		testResponse.name == cityResp
		testResponse.coord == cityCoord

		where:
		cityReq||cityResp|cityCoord
		"Kharkiv,UA"||"Kharkiv"|['lat' : 49.99, 'lon' : 36.23]
		"London,GB"||"London"|['lat' : 51.51, 'lon' : -0.13]
	}
}