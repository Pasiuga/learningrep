from behave import *


@given('I am on home page')
def i_am_on_home_page(context):
    context.driver.get("http://www.python.org")


@when('I search for {text}')
def i_search_for_text(context, text):
    elem = context.driver.find_element_by_name("q")
    elem.send_keys(text)


@then('I should see list of matching products in search results')
def step_i_should_see_list(context):
    assert "No results found." not in context.driver.page_source
