package dna

import spock.lang.Specification
import spock.lang.Shared


class DnaSpTest extends Specification {
    @Shared private Dna dna1, dna2

    def setupSpec(){
        dna1 = new Dna("aaaaaaaaaaa")
        dna2 = new Dna("ttttttttttt")
    }
    def cleanupSpec() {
        dna1 = null
        dna2 = null
    }

    def 'Check working getEnergyWindow method'(){
        when:
        double energyTest0 = dna1.getEnergyWindow(0)
        double energyTest1 = dna1.getEnergyWindow(1)
        double energyTest2 = dna2.getEnergyWindow(0)
        double energyTest3 = dna2.getEnergyWindow(1)

        then:
        energyTest0 == energyTest1
        energyTest2 != energyTest3
    }
}