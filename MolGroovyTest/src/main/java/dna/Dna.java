package dna;

public class Dna {
	public String sequence;
	public double energyTotal;
	public int sequenceLenght;
	private double energyCurr, energy;
	private String seq1, sequenceWindow;
	private char nucleotide;

	public Dna(String sequence) {
		this.sequence = sequence;
		energyTotal = 0.0;
		sequenceWindow = "";
		sequenceLenght = sequence.length();
	}

	private double getEnergySingle(String seq, int position) {
		energyCurr = 0.0;
		nucleotide = seq.charAt(position);
		try {
			if (nucleotide == 'a' | nucleotide == 't') {
				energyCurr = -68.8;
			} else if (nucleotide == 'c' | nucleotide == 'g') {
				energyCurr = -115.1;
			} else {
				throw new MyException("Incorrect nucleotide");
			}
		} catch (MyException e) {
			System.out.println(e.getMessage());
		}
		return energyCurr;
	}

	private double getEnergyStec(String seq, int position) {
		energyCurr = 0.0;
		seq1 = seq.substring(position, (position + 2));
		switch (seq1) {
		case "aa":
			energyCurr = -4.65;
			break;
		case "ac":
			energyCurr = -7.58;
			break;
		case "at":
			energyCurr = -5.61;
			break;
		case "ag":
			energyCurr = -4.44;
			break;
		case "ta":
			energyCurr = -0.8;
			break;
		case "tc":
			energyCurr = -5.99;
			break;
		case "tt":
			energyCurr = -4.64;
			break;
		case "tg":
			energyCurr = -2.3;
			break;
		case "ga":
			energyCurr = -5.98;
			break;
		case "gc":
			energyCurr = -9.08;
			break;
		case "gt":
			energyCurr = -7.57;
			break;
		case "gg":
			energyCurr = -6.02;
			break;
		case "ca":
			energyCurr = -2.3;
			break;
		case "cc":
			energyCurr = -6.02;
			break;
		case "ct":
			energyCurr = -4.43;
			break;
		case "cg":
			energyCurr = -3.8;
			break;
		}
		return energyCurr;
	}

	public double getEnergyWindow(int pos) {
		energy = 0.0;
		sequenceWindow = sequence.substring(pos, (pos + 10));
		for (int j = 0; j < 10; j++) {
			if (j == 0) {
				energy += getEnergySingle(sequenceWindow, j) + getEnergyStec(sequenceWindow, j);
			} else if (0 < j & j < (sequenceWindow.length() - 1)) {
				energy += getEnergySingle(sequenceWindow, j) + getEnergyStec(sequenceWindow, j)
						+ getEnergyStec(sequenceWindow, (j - 1));
			} else if (j == (sequenceWindow.length() - 1)) {
				energy += getEnergySingle(sequenceWindow, j) + getEnergyStec(sequenceWindow, (j - 1));
			}
		}
		return energy / 10;
	}

	public void getEnergySpecific() {
		energyTotal = energyTotal / (sequenceLenght - 9);
	}

}
