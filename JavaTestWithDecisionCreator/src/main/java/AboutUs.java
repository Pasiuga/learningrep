import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by pasiuga on 7/22/2016.
 */
public class AboutUs {
    private WebDriver driver;
    private String aboutUsTitle = "About us | Demoqa";

    public AboutUs (WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy (xpath = ".//*[@class='menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-156 current_page_item menu-item-158 active']/a")
    public WebElement aboutUsButtonActive;

    public void checkAboutUsPage(){
        Assert.assertTrue("No About us page", driver.getTitle().equals(aboutUsTitle));
    }

    public void checkAboutUsButtonActive(){
        Assert.assertTrue("About us button is inactive", aboutUsButtonActive.isDisplayed());
    }


}
