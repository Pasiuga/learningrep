import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pasiuga on 7/22/2016.
 */
public class Registration {
    private WebDriver driver;
    private Actions actions;
    private String myMessage;
    private Select countrySelect;
    private Select yearsSelect;

    public Registration (WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy (xpath = ".//*[@id='name_3_firstname']")
    public WebElement textboxFirstName;

    @FindBy (xpath = ".//*[@class='fieldset piereg_submit_button']/input")
    public WebElement submitButton;

    @FindBy (xpath = ".//*[@class='legend error']")
    public List<WebElement> legendError;

    @FindBy (xpath = ".//*[@value='dance']")
    public WebElement danceCheckbox;

    @FindBy (xpath = ".//*[@value='cricket ']")
    public WebElement cricketCheckbox;

    @FindBy (xpath = ".//*[@id='dropdown_7']")
    public WebElement countryDropdown;

    @FindBy (xpath = ".//*[@id='yy_date_8']")
    public WebElement yearDateOfBirth;


    public void addTextToFirstNameTextbox(String myMessage){
        textboxFirstName.click();
        textboxFirstName.sendKeys(myMessage);
        Assert.assertEquals("Text is not desplayed", myMessage, textboxFirstName.getAttribute("value"));
    }

    public void clickSubmitButton(){
        try {
            submitButton.click();
        }catch (NoSuchElementException e) {
            Assert.fail("Submit button is absent");
        }
    }

    public void checkLegendErrorMessages(int summa){
        Assert.assertEquals("Some '* This field is required' are absent", summa, legendError.size());
    }

    public void checkDanceCheckbox(){
        try{
            danceCheckbox.click();
        }catch (NoSuchElementException e) {
            Assert.fail("Dance checkbox is not present");
        }
    }

    public void checkDanceCheckboxStatus(){
        Assert.assertTrue("Dance checkbox is not checked", danceCheckbox.isSelected());
    }

    public void checkCricketCheckbox(){
        try{
            cricketCheckbox.click();
        }catch (NoSuchElementException e) {
            Assert.fail("Cricket checkbox is not present");
        }
    }

    public void checkCricketCheckboxStatus(){
        Assert.assertTrue("Dance checkbox is not checked", cricketCheckbox.isSelected());
    }

    public void checkCountryDropdownMenu(){
        try {
            countryDropdown.click();
        }catch (NoSuchElementException e) {
            Assert.fail("Country dropdown menu is not present");
        }
    }

    public void selectCountryDropdownMenu(String country){
        countrySelect = new Select(countryDropdown);
        countrySelect.selectByValue(country);
        Assert.assertEquals("Bahamas is not selected", country, countrySelect.getFirstSelectedOption().getText());
    }

    public void checkYearsFromTo(int from, int to){
        yearsSelect = new Select(yearDateOfBirth);
        List<WebElement> yearsList = yearsSelect.getOptions();
        int fromAct = Integer.valueOf(yearsList.get(1).getText());
        Assert.assertEquals("From year is not present", from, fromAct);
        int toAct = Integer.valueOf((yearsList.get(yearsList.size()-1).getText()));
        Assert.assertEquals("To year is not presented", to, toAct);
    }
}
