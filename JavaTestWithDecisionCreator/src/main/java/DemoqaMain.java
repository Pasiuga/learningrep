import org.junit.Assert;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by pasiuga on 7/22/2016.
 */
public class DemoqaMain {
    private WebDriver driver;

    public DemoqaMain (WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy (xpath = ".//*[@id='menu-item-158']/a")
    public WebElement aboutUsButton;

    @FindBy (xpath = ".//*[@id='menu-item-374']/a")
    public WebElement registrationLink;

    @FindBy (xpath = ".//*[@id='menu-item-147']/a")
    public WebElement menuLink;

    public void clickAboutUsLink(){
        try{
            aboutUsButton.click();
        } catch (NoSuchElementException e) {
            Assert.fail("About us button is absent");
        }
    }

    public void clickRegistrationLink(){
        try {
            registrationLink.click();
        } catch (NoSuchElementException e) {
            Assert.fail("Registration link is not present");
        }
    }

    public void clickMenuLink(){
        try {
            menuLink.click();
        } catch (NoSuchElementException e){
            Assert.fail("Menu link is not present");
        }
    }


}
