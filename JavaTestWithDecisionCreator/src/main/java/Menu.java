import org.junit.Assert;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by pasiuga on 7/26/2016.
 */
public class Menu {
    private WebDriver driver;
    private Actions actions;

    public Menu (WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);
        this.actions = actions;
    }

    @FindBy (xpath = ".//*[@id='ui-id-2']")
    public WebElement withSubmenuLink;

    @FindBy (xpath = ".//div[@id='tabs-2']/div[@class='inside_contain']/div[@id='navigate']/ul/li[4]/a")
    public WebElement faqLink;

    @FindBy (xpath = ".//div[@id='tabs-2']/div[@class='inside_contain']/div[@id='navigate']/ul/li[4]/ul/li[1]/a")
    public WebElement faqSubmenuLink;

    public void clickWithSubmenuLink(){
        try {
            withSubmenuLink.click();
        } catch (NoSuchElementException e){
            Assert.fail("With submenu link is absent");
        }
    }

    public void navigateToFaqLink(){
        try {
            faqLink.click();
        }catch (NoSuchElementException e){
            Assert.fail("FAQ link is absent");
        }
    }

    public void checkSubmenuFromFaqMenu(){
        Assert.assertTrue("Submenu for FAQ link is absent", faqSubmenuLink.isDisplayed());
    }


}
