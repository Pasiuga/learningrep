import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Created by pasiuga on 7/22/2016.
 */
public class DemTest extends  DecisionCreator{
    private static WebDriver driver;
    private static DemoqaMain demoqaMainPage;
    private static AboutUs aboutUsPage;
    private static Registration registrationPage;
    private static Menu menuPage;
    private static String demoqaUrl;

    static File f;
    static BufferedWriter bw;

    @BeforeClass
    public static void setUp() throws Exception{
        driver = new FirefoxDriver();
        demoqaUrl = "http://demoqa.com/";
        driver.manage().window().maximize();
        driver.get(demoqaUrl);
        demoqaMainPage = new DemoqaMain(driver);
        aboutUsPage = new AboutUs(driver);
        registrationPage = new Registration(driver);
        menuPage = new Menu(driver);

        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
        Date date = new Date();
        f = new File("test_results.html");
        bw = new BufferedWriter(new FileWriter(f, true));
        bw.write("<html><body>");
        bw.write("<h1>Test Case Status - " + dateFormat.format(date) + "</h1>");
    }

    @Before
    public void setUpStart() throws Exception{
        driver.get(demoqaUrl);
        driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
    }

    @AfterClass
    public static void tearDown() throws Exception{
        driver.close();
        for (int i = 0; i < DecisionCreator.testDecision.size(); i++){
            bw.write("<br>" + DecisionCreator.testDecision.get(i) + "</br>");
        }
        bw.write("</body></html>");
        bw.close();
    }

    @Test
    public void test1() throws Exception{
        demoqaMainPage.clickAboutUsLink();
        aboutUsPage.checkAboutUsPage();
    }

    @Test
    public void test2()throws Exception{
        demoqaMainPage.clickAboutUsLink();
        aboutUsPage.checkAboutUsButtonActive();
    }

    @Test
    public void test3() throws Exception{
        demoqaMainPage.clickRegistrationLink();
        registrationPage.addTextToFirstNameTextbox("Volodymyr");
    }

    @Test
    public void test4() throws Exception{
        demoqaMainPage.clickRegistrationLink();
        registrationPage.clickSubmitButton();
        registrationPage.checkLegendErrorMessages(8);
    }

    @Test
    public void test5() throws Exception{
        demoqaMainPage.clickRegistrationLink();
        registrationPage.checkDanceCheckbox();
        registrationPage.checkCricketCheckbox();
        registrationPage.checkDanceCheckboxStatus();
        registrationPage.checkCricketCheckboxStatus();
    }

    @Test
    public void test6() throws Exception{
        demoqaMainPage.clickRegistrationLink();
        registrationPage.selectCountryDropdownMenu("Bahamas");
    }

    @Test
    public void test7() throws Exception{
        demoqaMainPage.clickRegistrationLink();
        registrationPage.checkYearsFromTo(2018, 1950);
    }

    @Test
    public void test8() throws Exception{
        demoqaMainPage.clickMenuLink();
        menuPage.clickWithSubmenuLink();
        menuPage.navigateToFaqLink();
        menuPage.checkSubmenuFromFaqMenu();
    }

}
