/**
 * Created by pasiuga on 2/6/2017.
 */
class PatientClaim {
    private int patientId
    private Date svcDate = new Date()

    PatientClaim() {
        this.patientId = patientId
        this.svcDate = svcDate
    }

    def setPatientId(String patientIdStr){
        this.patientId = Integer.valueOf(patientIdStr.replace('"',''))
    }

    def setDate(String svcDateStr){
        this.svcDate = Date.parse('mm/dd/yyyy', svcDateStr.replace('"', ''))
    }
}
