import spock.lang.Specification
import spock.lang.Shared

/**
 * Created by pasiuga on 2/6/2017.
 */
class SortTest extends Specification {
    @Shared
            myFile, initList,  initPatientClaim, initPatientClaims, sortedPatientClaims

    def "Check that list is sorted"() {
        when:
        initPatientClaims = []
        sortedPatientClaims = []
        initList = []
        myFile = new File("Medical_claims+(input).csv")
        myFile.each { initList.add(it)}
        initList.remove(0)
        initList.each {
            initPatientClaim = new PatientClaim()
            initPatientClaim.setPatientId(it.split(',')[0])
            initPatientClaim.setDate(it.split(',')[5])
            initPatientClaims.add(initPatientClaim)
        }
        initPatientClaims.each { println(it.patientId + " " + it.svcDate.format('mm-dd-yyyy')) }
        sortedPatientClaims = initPatientClaims.clone()
        println()
        sortedPatientClaims.sort{ it.svcDate }
        sortedPatientClaims.sort{ it.patientId }
        sortedPatientClaims.each { println(it.patientId + " " + it.svcDate.format('mm-dd-yyyy')) }

        then:
        initPatientClaims == sortedPatientClaims
    }

}